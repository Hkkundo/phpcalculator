<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class PowCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {base : The %s number} {exp : The %s number}',
            $commandVerb,
            $this->getDescription1Verb(),
            $this->getDescription2Verb()
        );
        $this->description = sprintf('%s the given Numbers', ucfirst($this->getDescription2Verb()));
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }


    protected function getCommandPassiveVerb(): string
    {
        return 'powed';
    }
    
    protected function getDescription1Verb(): string
    {
        return 'base';
    }

    protected function getDescription2Verb(): string
    {
        return 'exponent';
    }

    public function handle(): void
    {
        $base = $this->getInput()['base'];
        $exp = $this->getInput()['exp'];
        $description = $this->generateCalculationDescription($base, $exp);
        $result = $this->calculateAll($base, $exp);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput()
    {
        return [
            'base' => $this->argument('base'),
            'exp' => $this->argument('exp')
        ];
    }

    protected function generateCalculationDescription($base, $exp): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return $base.$glue.$exp;
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll($base, $exp)
    {
        if ($base <= 0) {
            return $base;
        }

        if ($exp <= 0) {
            return $exp;
        }

        return $this->calculate($base, $exp);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return pow($number1, $number2);
    }
}
