<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var array
     */
    protected $headers;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s',
            $commandVerb,
        );
        $this->description = sprintf('Clear saved %s', ucfirst($this->getDescriptionVerb()));
        parent::__construct();
    }

    protected function getDescriptionVerb(): string
    {
        return 'history';
    }

    protected function getCommandVerb(): string
    {
        return 'history:clear';
    }

    public function handle(): void
    {
        
    }
}
