<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var array
     */
    protected $headers;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {commands* : Filter %s by commands}',
            $commandVerb,
            $this->getDescriptionVerb()
        );
        $this->description = sprintf('Show calculator %s', ucfirst($this->getDescriptionVerb()));
        parent::__construct();
    }

    protected function getDescriptionVerb(): string
    {
        return 'history';
    }

    protected function getCommandVerb(): string
    {
        return 'history:list';
    }

    protected function getHeader(): array
    {
        return ['No', 'Command', 'Description', 'Result', 'Output', 'Time'];
    }

    public function handle(): void
    {
        $commands = $this->getInput();
    }

    protected function getInput(): array
    {
        return $this->argument('commands');
    }
}
